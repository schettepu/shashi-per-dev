public with sharing class diffInMilliSeconds{

    /*
        CreatedBy: Shashi R
        Date: 
        Purpose: creating invocable class/method to get output for various inputs
    */

    @Description('Request Structure')
    public class requestStruct{
        @InvocableVariable(required=true) 
        public DateTime startDate;
        @InvocableVariable(required=true) 
        public DateTime endDate;
    }//end public class requestStruct{

    @Description('Response Structure')
    public class responseStruct{
        @InvocableVariable  
        public long diffResult;
    }//end @Description('Response Structure')

    
    @InvocableMethod(Description='method to return difference in milli seconds')
    public static List<responseStruct> getMilliSecondDiff(List<requestStruct> request){
        System.debug('****Received values: '+ '****startDate: ' +request[0].startDate + '****endDate: '+request[0].endDate);
        long diffinms =0;
        //fetch org default businesshours details
        Id businessHoursId = [Select Id from BusinessHours where isdefault = True].Id;
        diffinms = BusinessHours.diff(businessHoursId,request[0].startDate,request[0].endDate);
        List<responseStruct> rsList = new List<responseStruct>();
        responseStruct rs = new responseStruct();
        rs.diffResult = diffinms;
        rsList.add(rs);
        System.debug('**** Difference in milliseconds: ' + rsList);
        return rsList; //output in ms - long data type
    }//end public static responseStruct getMilliSecondDiff(requestStruct request){
}//end public with sharing class invockableclasses{