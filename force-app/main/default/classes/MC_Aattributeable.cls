/*Interface to Allow inner classes to be used as responses in Aura enabled methods. Any Inner Class that implements 
this interface can be used as a response in Aura enabled methods.
 */
public Interface MC_Aattributeable {
}