public class WSNeustar 
//this properties are created to access values from VF page

{
  /*  public integer i_Telephone{get;set;}
    public string s_CustomerID{get;set;}
    public map<string,string> mp_response;
    public static String s_xmlContent {get;set;}
    public WSNeustar(){
        
    }
    //this is called from vf page save button
    public  pagereference save(){
        //initially service keys list is generated based on input values from vfpage
        list<targusinfoComWsGetdata.ServiceKeyType> lst_servicekey=new list<targusinfoComWsGetdata.ServiceKeyType>();
        targusinfoComWsGetdata.ServiceKeyType objservicekey;
        //map is created for xml creation for http callout
        map<integer,string> mp_values=new map<integer,string>();
        //if Telephone is not null then respective code of phonenumber as per spec doc  is 
        //fetched from custom settings and given to  servicekeytype obj as parameters
        if(i_Telephone!=null){
            objservicekey=new  targusinfoComWsGetdata.ServiceKeyType();
            ServiceTokens__c objtoken = ServiceTokens__c.getValues('PhoneNumber');
            objservicekey.id=integer.valueof(objtoken.token__c);
            objservicekey.value=string.valueof(i_Telephone);
            mp_values.put(integer.valueof(objtoken.token__c),string.valueof(i_Telephone));
            lst_servicekey.add(objservicekey);
        }
        
        if(s_CustomerID!=null){
            objservicekey=new  targusinfoComWsGetdata.ServiceKeyType();
            ServiceTokens__c objtoken = ServiceTokens__c.getValues('CustomerID');
            objservicekey.id=integer.valueof(objtoken.token__c);
            objservicekey.value=string.valueof(s_CustomerID);
            mp_values.put(integer.valueof(objtoken.token__c),string.valueof(s_CustomerID));
            lst_servicekey.add(objservicekey);
        }
        mp_values.put(206,'MATCHCODE|CLIENTDATA');
        
        //soap call
        // neustar_WebserviceCall(label.NeustarUserid,label.neustarpassword,lstservicekey);
        //restcall
        SendHttpcall(mp_values,1,1722,1);
        return null;
    }
    public void neustar_WebserviceCall(string username,string password,  list<targusinfoComWsGetdata.ServiceKeyType> lstservicekey){
        
        //origination details
        targusinfoComWsGetdata.OriginationType objtargusorigin=new targusinfoComWsGetdata.OriginationType();
        objtargusorigin.username=username;
        objtargusorigin.password=password;
        //array of service
        targusinfoComWsGetdata.ArrayOfServiceKeyType objservicekeyarray=new targusinfoComWsGetdata.ArrayOfServiceKeyType();
        objservicekeyarray.serviceKey=lstservicekey;
        //array of queries
        targusinfoComWsGetdata.BatchQueryType objbatchquery= new targusinfoComWsGetdata.BatchQueryType ();
        objbatchquery.serviceKeys=objservicekeyarray;
        list<targusinfoComWsGetdata.BatchQueryType> lst_bquery= new  list<targusinfoComWsGetdata.BatchQueryType>();
        lst_bquery.add(objbatchquery);
        
        targusinfoComWsGetdata.ArrayOfBatchQueryType objbatchqueryarray=new   targusinfoComWsGetdata.ArrayOfBatchQueryType();
        objbatchqueryarray.query=lst_bquery;
        //ArrayOfInt
        // as we are looking for Element id 1722 following details given as parameters in arrayofint 
        list<integer> lst_Elementid=new list<integer>();
        lst_Elementid.add(integer.valueof(label.elementid));
        targusinfoComWsGetdata.ArrayOfInt objarrayint=new targusinfoComWsGetdata.ArrayOfInt();
        objarrayint.id=lst_Elementid;
        
        // creating object for client soap call
        targusinfoComWsGetdata.ClientSoap stub=new targusinfoComWsGetdata.ClientSoap();
        //in clientsoap class there is a method by name batch query here
        // in this webservice is called so expected parameters are prepared and send to this method as request
        if(!Test.isRunningTest()){
            targusinfoComWsGetdata.BatchResponseMsgType objres= stub.batchQuery(objtargusorigin,'CustomerServiceID',1,objarrayint,objservicekeyarray,objbatchqueryarray);
        }
        
    }
    public static HttpResponse SendHttpcall(map<integer,string> mServicekeys,integer transid,integer elementid,integer serviceid){
        
        string xmlstring=GenerateXMLRequest(mServicekeys,transid,elementid,serviceid);
        Http http = new Http();
        HttpRequest req = new HttpRequest();
        req.setEndpoint('http://TARGUSinfo.com');
        req.setMethod('GET'); 
        req.setBody(xmlstring);
        HttpResponse res ;
        if(!Test.isRunningTest()){
            res  = http.send(req);
            s_xmlContent=res.getBody();
            Dom.Document doc1 = new Dom.Document();
            doc1.load(res.getBody());
            Dom.XMLNode Envelope = doc1.getRootElement();
        }
        else{
            res=null;
        }
        return res;
    }
    
    public static string GenerateXMLRequest(map<integer,string> mServicekeys,integer transid,integer elementid,integer serviceid){
        string xmlstring;
        DOM.Document doc = new DOM.Document();
        dom.XmlNode node_Query = doc.createRootElement('query ', null, null);
        node_Query.setAttribute('xmlns','http://TARGUSinfo.com/WS-GetData');
        dom.XmlNode node_origin= node_Query.addChildElement('origination',null, null);
        dom.XmlNode  body1_username= node_origin.addChildElement('username',null, null);
        body1_username.addTextNode(label.NeustarUserid);
        dom.XmlNode  body1_password= node_origin.addChildElement('password',null, null);
        body1_password.addTextNode( label.neustarpassword);
        dom.XmlNode node_Serviceid= node_Query.addChildElement('serviceId',null, null);
        node_Serviceid.addTextNode(string.valueof(serviceid));
        dom.XmlNode node_transid= node_Query.addChildElement('transId',null, null);
        node_transid.addTextNode(string.valueof(transid));
        dom.XmlNode node_elementid= node_Query.addChildElement('elements',null, null);
        dom.XmlNode subbody4= node_elementid.addChildElement('id', null,null);
        subbody4.addTextNode(string.valueof(elementid));
        dom.XmlNode body5= node_Query.addChildElement('serviceKeys',null, null);
        for (integer keys : mServicekeys.keySet()){
            dom.XmlNode subbody5 = body5.addChildElement('serviceKey',null, null);
            dom.XmlNode sub_id_body5  =subbody5.addChildElement('id', null, null);
            sub_id_body5.addTextNode(string.valueof(keys));
            dom.XmlNode sub_value_body5  =subbody5.addChildElement('value', null, null);
            sub_value_body5.addTextNode(mServicekeys.get(keys));
            // subbody5.setAttribute('value', label.NeustarUserid);
        }
        xmlstring= doc.toXmlString(); 
        
        system.debug('xmlrequest==='+xmlstring);
        
        return xmlstring;
    }
    public  void parseXML(DOM.XMLNode node) {
        if (node.getNodeType() == DOM.XMLNodeType.ELEMENT) {
            system.debug(node.getName());
            mp_response.put(node.getName(),node.gettext());
        }
        for (Dom.XMLNode child: node.getChildElements()) {
            parseXML(child);
        }
    }*/
}