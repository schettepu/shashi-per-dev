/**
 * Database.schedule('My First Schedule', '0 5 * * * ?', new schduleBatchOfUser())
 * */
global class schduleBatchOfAccount implements Schedulable
{
    global void execute(SchedulableContext ctx)
    {
        UpdateAcctBatch p = new UpdateAcctBatch();
        Database.executeBatch(p);
        String sch1 = '0 5 * * * ?';
      
        schduleBatchOfAccount sch2 = new schduleBatchOfAccount ();
        system.schedule('Every Hour plus 0 min', sch1, sch2);
   }   
}