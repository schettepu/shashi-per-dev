@isTest
global class NeustarMockApiTest implements HttpCalloutMock {
    global HTTPResponse respond(HTTPRequest req) {
        System.assertEquals('GET', req.getMethod());
        HttpResponse res = new HttpResponse();
        res.setBody(Getresponsexml());
        res.setStatusCode(200);
        return res;
    }
    
    public string Getresponsexml(){
        string xmlstring;
        DOM.Document doc = new DOM.Document();
        dom.XmlNode products = doc.createRootElement('response ', null, null);
        dom.XmlNode body3= products.addChildElement('transId',null, null);
        body3.addTextNode(string.valueof(1));
        dom.XmlNode body2= products.addChildElement('errorcode',null, null);
        body2.addTextNode(string.valueof(0));
        dom.XmlNode subprod= products.addChildElement('result',null, null);
        dom.XmlNode body4= subprod.addChildElement('elements',null, null);
        dom.XmlNode subbody4= body4.addChildElement('id', null,null);
        subbody4.addTextNode(string.valueof(1722));
        dom.XmlNode subbody5= body4.addChildElement('errorCode', null,null);
        subbody5.addTextNode(string.valueof(0));
        dom.XmlNode subbody6= body4.addChildElement('value', null,null);
        subbody6.addTextNode('1  11    0921306765 0 0 04YYYCA7147           1000000099HBB        3570  Carmel        Mountain             Rd                San Diego');
        xmlstring= doc.toXmlString(); 
        system.debug('===xmlstring'+xmlstring);
        return xmlstring;
    }    
}