public class MC_Utility {

    
    
    
        public static User currentUser {
        get {
            if (currentUser == null) {
                currentUser = [
                        SELECT Id, Name, Phone, Email, SmallPhotoUrl, UserType,
                                Portal_Country__c, Portal_Locale__c, 
                                Country__c, LocaleSidKey, LanguageLocaleKey,
                                AccountId, ContactId, Contact.Email, Contact.Phone
                        FROM User
                        WHERE Id = :UserInfo.getUserId()
                ];
            }
            return currentUser;
        }
        private set;
    }
    
    
    public static TA_Country__c getCountrySettings(String countryName) {
        System.debug('countryName:' + countryName);
        TA_Country__c countrySettings = [
                SELECT Name FROM TA_Country__c
                WHERE country_Code__c = :countryName OR Name = :countryName
                LIMIT 1
        ];
        return countrySettings;
    }
    public static String getLanguageCode(String locale) {
        String languageCode = '';
        if (locale.contains('_')) {
            String[] locale1 = locale.split('_');
            languageCode = locale1[0].toLowerCase();
        }
        else {
            languageCode = locale.toLowerCase();
        }
        return languageCode;
    }

}