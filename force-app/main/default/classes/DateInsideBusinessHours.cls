public with sharing class DateInsideBusinessHours {

    /*
        CreatedBy: Shashi R
        Date: 
        Purpose: creating invocable class/method to determine if a given date is inside organization&#39;s business hours
    */

    public class inputDate{
        @InvocableVariable
        public DateTime givenDateTime;
        @InvocableVariable
        public String businessHoursId;
    }

    public class responseoutputID{
        @InvocableVariable
        public Boolean dateInsideBusinessHours;
    }

    @InvocableMethod(label='DateInBusHours' description='determine if a given date is inside organizations business hours')
    public static List<responseoutputID> DateInBusHours (List<inputDate> inputParams) {
        System.debug('****Received Date to check: ' + inputParams[0].givenDateTime);
        System.debug('****Received businessHoursId to check: ' + inputParams[0].businessHoursId);
        
        //initialize response
        List<responseoutputID> responseList = new List<responseoutputID>();
        Boolean checkResult;
        if(inputParams != null && inputParams.size()> 0){
            checkResult = BusinessHours.isWithin(inputParams[0].businessHoursId, inputParams[0].givenDateTime);
            System.debug('*****checkResult: ' + checkResult);   
        }//end if   
        
        //form output
        responseoutputID rs = new responseoutputID();
        rs.dateInsideBusinessHours = checkResult;
        responseList.add(rs);
        return responseList;
    }//end public static List<responseoutputID> DateInBusHours (List<inputDate> inputParams) {
}