public with sharing class Equipmentrecords{
    public List<Equipment__c> EquipmentRecords {get; set;}
   public boolean displayPopup {get; set;}

    public Equipmentrecords(){
        EquipmentRecords= [
            SELECT Name, Price__c, Type__c, Vendor__c FROM Equipment__c
           ];
    }
     public PageReference save(){
       
        upsert EquipmentRecords;      
        PageReference reRend = new PageReference('/lightning/r/Equipment__c/a0K4100000CtkbGEAR/view');
        reRend.setRedirect(true);
        return reRend;
    }
}