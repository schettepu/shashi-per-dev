public with sharing class UMUCCtrl {
	
    public static string FizzBuzz(Integer inpInteger){
        	System.debug('***received integer: '+ inpInteger);
        String result;
        if(Math.mod(inpInteger,3) ==0 && Math.mod(inpInteger,5)==0){
            result = 'FizzBuzz';
        } else if(Math.mod(inpInteger,3) ==0){
            result = 'Fizz';
        }else if(Math.mod(inpInteger,5) ==0){
            result = 'Buzz';        
        }else{
            result = String.ValueOf(inpInteger);
        }//end if
        system.debug('***result: ' + result);
      return result;
   }//end public string FizzBuzz ( String inpInteger){
}