public with sharing class PromiseAuraTestController {
	@AuraEnabled
   public static List<WeatherInfo> getWeatherInfo(){
      return new List<WeatherInfo>{
         new WeatherInfo('Monday', 'Cloudy'),
         new WeatherInfo('Tuesday', 'Cloudy'),
         new WeatherInfo('Wednesday', 'Cloudy'),
         new WeatherInfo('Thursday', 'Cloudy'),
         new WeatherInfo('Friday', 'Sunny!'),
         new WeatherInfo('Saturday', 'Sunny!'),
         new WeatherInfo('Sunday', 'Sunny!')
      };
   }
 
   public class WeatherInfo {
      @AuraEnabled
      public String dayName;
 
      @AuraEnabled
      public String description;
 
      public WeatherInfo(String dayName, String description){
         this.dayName = dayName;
         this.description = description;
      }
   }
}