@isTest
public class LeadProcessorTest {
    @isTest static void TestBatch () {
        
        List<Lead> LeadList = new List<Lead>();
        for (integer i = 0; i<200;i++)
        {
            Lead Ld = new Lead();
            Ld.FirstName = 'FirstName';
            Ld.LastName = 'LastName'+i;
            Ld.Company = 'demo'+i;
            LeadList.add(Ld);
        }
        
        insert LeadList;
        Test.startTest();
        LeadProcessor  LdProc = new LeadProcessor();
        DataBase.executeBatch(LdProc);
        Test.Stoptest();
    }
}