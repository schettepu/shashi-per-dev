public class ConsultationController {
//added by shashi - testing
    @AuraEnabled
    public static consultation__c saveConsultationRecord(consultation__c consultRecord){
        System.debug('****Received lightning wrapper: ' + consultRecord);
        Consultation__c cons = new consultation__c();
        cons.Career__c = '';
        cons.Program_of_Interest__c = '';
        cons.Skills__c = '';
        
        if(consultRecord != null){
            cons = consultRecord;
        }
        try{
            insert cons;           
            System.debug('****inserted Cons  :' +cons);
        }catch(exception e){
            system.debug('do nothing: ' + e.getMessage());
        }
        return cons;
    }    
    //to dynamically populate the campus and program type picklist values on program career tab
    public static Map<String,set<String>> CampusToProgramTypeMap = new Map<String,set<String>>();
    @AuraEnabled
    public static List<String> populateCampusValues(){
        system.debug('****fetch dynamic values');
        List<Account> termlist = [select ID,Name,Phone,Active__c from Account where EAPP_CONTROL_FLAG__C = 'Y'];
        set<String> campusSet = new set<string>();
        List<String> campusList = new List<String>();
        Map<Id,Account> aptMap = new Map<Id,Account>();
       // Map<String,set<String>> CampusToProgramTypeMap = new Map<String,Set<String>>();
        if(termlist.size() >0){
            for(Account ap : termlist){
                campusSet.add(ap.Name);
                aptMap.put(ap.Id,ap);
            }
        }
        system.debug('****campus set: ' + campusSet);
        if(campusSet.size() > 0){
            for(Account aptrecord: aptMap.Values()){
                if(!CampusToProgramTypeMap.containsKey(aptrecord.Name)){
                    set<String> programTypeList = new set<String>();
                    programTypeList.add(aptrecord.Phone);
                    CampusToProgramTypeMap.put(aptrecord.Name,programTypeList);
                }else {
                    CampusToProgramTypeMap.get(aptrecord.Name).add(aptrecord.Phone);
                }
            }
        }//end if
        
        system.debug('****Initial campus to programtype map : ' + CampusToProgramTypeMap);
        campusList.addAll(campusSet);
        system.debug('****campus List: ' + campusList);
        return campusList;
    }//end method
    //fetch program type values dynamically based on the campus value chosen

    @AuraEnabled
    public static List<String> populateprogramTypeValues(String campusName){
        system.debug('****received campus Name: '+ campusName);
        populateCampusValues();
        List<String> campusNameToProgramTypeList = new List<String>();
        if(campusName != null){
            if(CampusToProgramTypeMap.containsKey(campusName)){
                campusNameToProgramTypeList.addAll(CampusToProgramTypeMap.get(campusName));
            }
        }//end if
        system.debug('****campusNameToProgramTypeList: ' + campusNameToProgramTypeList);
        campusNameToProgramTypeList.sort();
        return campusNameToProgramTypeList;
    }//end public static List<String> populateprogramTypeValues(){
}