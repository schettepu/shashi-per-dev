global class errorLogRecordsProcessingBatch implements Database.Batchable<sObject>{
    List<error_log__c> errorRecordsList=new List<Error_log__c>();
    string emailIdsToNotify = 'pmakam@atlassian.com';
    
    //start method - lets get all the error records created in last 1 hour
    global Database.QueryLocator Start(Database.BatchableContext dc){
        String errorLogRecordsListQuery = 'Select Id,name, Error_log_Message__c from Error_log__c a where a.createdDate = Today';
        return Database.getQueryLocator(errorLogRecordsListQuery);
    }
    //AND HOUR_IN_DAY(a.createdDate) <1
    
    //execute method - lets process the records if found any - created in last 1 hour
    global void execute(Database.BatchableContext dc, List<Error_log__c> scope){
        //do nothing here - just add error records to global list
        system.debug('****scope.size():' + scope.size());
        for(Error_log__c elog: scope){
            errorRecordsList.add(elog);
        }
        
    }
    
    //finish method - lets place the notification email system here
    global void finish(Database.BatchableContext dc){
        system.debug('****errorRecordsList.size(): ' + errorRecordsList.size()); 
        if(errorRecordsList != null && errorRecordsList.size()>0){
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                mail.setToAddresses(new String[] {emailIdsToNotify});
                mail.setReplyTo('batch@gmail.com');
                mail.setSenderDisplayName('Batch Processing');
                mail.setSubject('webservice Error log notification Batch Process Completed');
                mail.setPlainTextBody('there are '+ errorRecordsList.size() + 'webservice Errors reported in last 1 hour');
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
         }
    }

}