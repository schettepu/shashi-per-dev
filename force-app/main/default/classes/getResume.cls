public class getResume {
    @AuraEnabled
    public static String getResumeURI (string filedId){
        string URI;
        ContentVersion cv =   [ SELECT Id, VersionData, FileExtension, Title FROM ContentVersion 
                               WHERE ContentDocumentId='0691K00000XUxLeQAL' ];
        
        
        
        blob myblob = cv.VersionData;
        
        System.debug('******myblob : ' +myblob);
        
        URI = 'data:application/pdf;base64,' + EncodingUtil.base64Encode(myblob);
        system.debug('URI   ::' +URI);
        
        
        return URI;
    }
    
     public static string fileURI {
        get {
            if(ApexPages.currentPage().getParameters().get('fileId') != null){
                system.debug('fileURI' +fileURI);
                system.debug('fileURI' +getResumeURI(ApexPages.currentPage().getParameters().get('fileId')));
                return getResumeURI(ApexPages.currentPage().getParameters().get('fileId'));
            }else{
                return null;
            }
            
        }
    }
}