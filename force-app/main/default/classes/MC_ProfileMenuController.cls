public with sharing class MC_ProfileMenuController {

	@AuraEnabled
	public static MenuDataWrapper init(String language){
		// if we didn't get a language from the Lightning component, grab it from the current user
		if(String.isBlank(language)){
			language = String.isNotBlank(MC_Utility.currentUser.Portal_Locale__c) ? MC_Utility.currentUser.Portal_Locale__c : MC_Utility.currentUser.LocaleSidKey;
		}

		return new MenuDataWrapper(language);
	}

	public class MenuDataWrapper implements MC_Aattributeable {
		@AuraEnabled public User currentUser { get; set; }
		@AuraEnabled public TA_Country__c currentSettings { get; set; }
		@AuraEnabled public Boolean isGuestUser { get; set; }
		@AuraEnabled public Boolean isDealerUser { get; set; }
		@AuraEnabled public Boolean isInternalUser { get; set; }
		@AuraEnabled public Boolean isCustomerUser { get; set; }
		@AuraEnabled public Boolean isProspectiveUser { get; set; }

		@AuraEnabled public String loginURL { get; set; }
		@AuraEnabled public String logoutURL { get; set; }

		public MenuDataWrapper(String language){
			String languageCode = MC_Utility.getLanguageCode(language);
			String countryCode = 'US';

			currentUser = MC_Utility.currentUser;
			currentSettings = MC_Utility.getCountrySettings(countryCode);
			/*isGuestUser = GC_Utility.isGuestUser();
			isDealerUser = GC_Utility.isDealerPortalUser();
			isInternalUser = GC_Utility.isInternalPortalUser();
			isCustomerUser = GC_Utility.isCustomerPortalUser();
			isProspectiveUser = GC_Utility.isProspectivePortalUser();
*/
			//loginURL = GC_SelfRegisterController.getLoginURL(language);

			// get the base site url minus the last /s so we can create the logout link
			//logoutURL = GC_Utility.getSiteBaseUrl(GC_Utility.MYCATFINANCIAL_BASE_URL_KEY, GC_Utility.MYCATFINANCIAL_SITE_NAME).removeEnd('/s')
				//	+ '/secur/logout.jsp'
					//+ (String.isNotBlank(loginURL) ? '?retUrl=' + loginURL : '');
		}
	}
}