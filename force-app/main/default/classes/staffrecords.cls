public class staffrecords  {
   public List<Staffing__c> accs {set;get;}
    public staffrecords(){
        accs = [select id, name,Gender__c,Person_email__c,Contact__c,Type_of_staffing__c,Record_number__c from Staffing__c limit 10];
}
 

public class staffrecordsExtension {

    private final Account acct;
    
    // The extension constructor initializes the private member
    // variable acct by using the getRecord method from the standard
    // controller.
    public staffrecordsExtension(ApexPages.StandardController stdController) {
        this.acct = (Account)stdController.getRecord();
    }

    public String getGreeting() {
        return 'Hello ' + acct.name + ' (' + acct.id + ')';
    }
}
 }