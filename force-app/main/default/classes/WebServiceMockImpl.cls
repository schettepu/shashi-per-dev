@isTest
global class WebServiceMockImpl implements WebServiceMock {
   global void doInvoke(
           Object stub,
           Object request,
           Map<String, Object> response,
           String endpoint,
           String soapAction,
           String requestName,
           String responseNS,
           String responseName,
           String responseType) {
     
       targusinfoComWsGetdata.batchQueryResponse_element respElement=new targusinfoComWsGetdata.batchQueryResponse_element();    
       response.put('response_x', respElement); 
   }
}