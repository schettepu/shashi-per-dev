global class RateCalculatorHandler {
    ///this properties are created to access values from VF page
    public string pckid{get;set;}
    public string sService{get;set;}
    public string sZipOrigination{get;set;}
    public string sZipDestination{get;set;}
    public string sPounds{get;set;}
     public string sOunces{get;set;}
    public string sContainer{get;set;}
  
    public string sSize{get;set;}
    public string sWidth{get;set;}
    public string sLength{get;set;}
    public string sHeight{get;set;}
    public string sGirth{get;set;}
    public string srate{get;set;}    
        
       
    public static List<String> XMLData{get;set;}
    public RateCalculatorHandler(){
        XMLData=new List<String>();
    }
    //webservice method is called from javascript by passing static xml content
    webservice static void getRateInfo(String path,string AccountId){ 
        Http http = new Http();
        HttpRequest req = new HttpRequest();
        req.setEndpoint(path);
        req.setMethod('GET');
        HttpResponse res = http.send(req);
        String xmlContent=res.getBody();
        System.debug(res.getBody());
        System.debug('#####XmlStreamReader ##11##');
        XmlStreamReader reader = res.getXmlStreamReader();
        System.debug('##########XML DATA##########'+res.getXmlStreamReader());
        XMLParser(res.getBody(),AccountId);
         
    }
//xml is parsed based on the response 
    public Static void  XMLParser(String strXml,string AccountId){
        System.debug('####Inside XMLParser Method########'+strXml);
        List<String> orgInfo=new List<String>();
        Dom.Document doc = new Dom.Document();
        doc.load(strXml);
        //Retrieve the root element for this document.
        Dom.XMLNode Envelope = doc.getRootElement();
        parseXML(Envelope,AccountId);
        //return orgInfo;
    }
    //calling from vfpage to generate xml and http rest call is made and response is parsed 
    public PageReference save(){
        string xmlstring;
        DOM.Document doc = new DOM.Document();
        dom.XmlNode products = doc.createRootElement('RateV4Request', null, null);
        products.setAttribute('USERID','483RESOU6374');
        dom.XmlNode body1= products.addChildElement('Package',null, null);
        body1.setAttribute('ID', pckid);
        body1.addChildElement('Service', null, null).addTextNode(sService);
        body1.addChildElement('ZipOrigination', null, null).addTextNode(sZipOrigination);
        body1.addChildElement('ZipDestination', null, null).addTextNode(sZipDestination);
        body1.addChildElement('Pounds', null, null).addTextNode(sPounds);
        body1.addChildElement('Ounces', null, null).addTextNode(sOunces);
        body1.addChildElement('Container', null, null).addTextNode(sContainer);
        body1.addChildElement('Size', null, null).addTextNode(sSize);
        body1.addChildElement('Width', null, null).addTextNode(sWidth);
        body1.addChildElement('Length', null, null).addTextNode(sLength);
        body1.addChildElement('Height', null, null).addTextNode(sHeight);
        body1.addChildElement('Girth', null, null).addTextNode(sGirth);
        
        xmlstring = doc.toXmlString(); 
         
        //system.debug('xxx'+xmlstring);
        //custom label is used for prod url
        string surl=system.label.RatecalculatorProdUrl;
        surl=surl+'?API=RateV4'+'&XML='+EncodingUtil.urlEncode(xmlstring,'UTF-8');
        system.debug('==='+surl);
        getRateInfo(surl);
        RateCal__c obj =new RateCal__c();
        obj.Rate__c='10';
        insert obj;
        //http://production.shippingapis.com/ShippingApi.dll?API=RateV4&XML=
        return null;   
    }
        //In this Http api request is made
   public void getRateInfo(String path){ 
        Http http = new Http();
        HttpRequest req = new HttpRequest();
        req.setEndpoint(path);
        req.setMethod('GET');
        HttpResponse res = http.send(req);
        String xmlContent=res.getBody();
        System.debug(res.getBody());
        System.debug('#####XmlStreamReader ##11##');
        XmlStreamReader reader = res.getXmlStreamReader();
       
       //XMLParser(res.getBody(),1);
        Dom.Document doc1 = new Dom.Document();
        doc1.load(res.getBody());
       //Retrieve the root element for this document.
        Dom.XMLNode Envelope = doc1.getRootElement();
        parseXML(Envelope);
    }
        //response xml is parsed in this method
     public  void parseXML(DOM.XMLNode node) {
        if (node.getNodeType() == DOM.XMLNodeType.ELEMENT) {
        system.debug(node.getName());
           
            if(node.getName()=='MailService'){
               system.debug('mail Service'+node.getText().trim());
               // node.getText().trim();
            }
            if(node.getName()=='Rate'){
                srate=node.getText();
                system.debug('==rate'+srate);
                
        }
       
        }
        for (Dom.XMLNode child: node.getChildElements()) {
            parseXML(child);
        }
    }
        // Picklist values in VF page
   public List<SelectOption> getserviceoptions() {
        List<SelectOption> Serviceoptions = new List<SelectOption>();
        Serviceoptions.add(new SelectOption('Retail Ground','Retail Ground'));
        Serviceoptions.add(new SelectOption('First Class','First Class'));
        Serviceoptions.add(new SelectOption('Priority','Priority'));
        Serviceoptions.add(new SelectOption('Priority Commercial','Priority Commercial'));
        Serviceoptions.add(new SelectOption('First Class Commercial',' First Class Commercial'));
 
       return serviceOptions;
    }
   

     public static void parseXML(DOM.XMLNode node,String AccountId) {
        if (node.getNodeType() == DOM.XMLNodeType.ELEMENT) {
        system.debug(node.getName());
            List<Account> accountList = new List<Account>();
           // accountList = [select id,Domestic_Rate__c,Domestic_Mailing_Service__c from Account where id=:AccountId];
            if(node.getName()=='MailService'){
               system.debug('mail Service'+node.getText().trim());
           //accountList[0].Domestic_Mailing_Service__c = node.getText().trim();
            }
            if(node.getName()=='Rate'){
                system.debug('rate'+node.getText().trim());
           //accountList[0].Domestic_Rate__c = Decimal.valueOf(node.getText().trim());
        }
        update accountList;
        }
        for (Dom.XMLNode child: node.getChildElements()) {
            parseXML(child,AccountId);
        }
    }
    
    
    
}