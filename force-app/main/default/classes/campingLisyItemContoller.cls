public class campingLisyItemContoller {
    @AuraEnabled
    public static List<Camping_Item__c>  CampingListMethod() {
        
        return [select id, Price__c, Quantity__c, Name , Packed__c FROM Camping_Item__c];
    }
    
}