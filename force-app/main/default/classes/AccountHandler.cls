public class AccountHandler {
    public static Account insertNewAccount(string Accountname) {
        Account acct = new Account(name = Accountname );
        //Acct.Name = Accountname;
        try { 
            insert Acct;
        } catch (DmlException e) {
            System.debug('A DML exception has occurred: ' +
                         e.getMessage());
            return null;
        }
        return acct;
    }
    @AuraEnabled
    public static Account saveAccount (Account acc, string ssn) {
        system.debug('**ssn :' +ssn);
        system.debug('**acc :' +acc);
        acc.ssn__c = ssn;
        upsert acc;
        //system.debug('**accId :' +acc.Id);
        String bodyStr=' '+ acc.Name +System.now();
        Attachment att=new Attachment();
        att.Body=Blob.valueOf(bodyStr);
        att.Name='Note' + System.now().format('yyyy_MM_dd_hh_mm_ss') + '.txt';
        att.parentId=Acc.id;
        insert att;
        return acc;
    }    
}